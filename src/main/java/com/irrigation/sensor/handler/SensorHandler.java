package com.irrigation.sensor.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.time.ZonedDateTime;
import java.util.Map;

/**
 * @author abdulmaroof
 */
public class SensorHandler implements RequestHandler<Map<String, Object>, Object> {

    public Object handleRequest(Map<String, Object> input, Context context) {
        context.getLogger().log(String.format("Running sensor job at %s for %s", ZonedDateTime.now(), input));
        return input.get("body");
    }
}
