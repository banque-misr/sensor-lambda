# Banue Misr - Sensor Lambda [Serverless]

---------------

### Pre Requisite:
1. Install maven following [these](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows) guidelines.

2. Install java 8 from [this](https://www.java.com/en/download/) link.

3. Install PostgreSQL from[this](https://www.postgresql.org/download/windows/) link

### Build
To build your application in the simply run: `mvn clean install` and it will execute all tests and if they exceed successfully then it will create build.

### Starting Application
To start your application, simply run:

    java -jar [name_of_jar]

To check your system has started successfully, Try accessing it from http://localhost:8080

**Note** 
1. To run the project, you can directly run without setting up on local. 
2. Project is using aws cloud services and everything will run without any configurations.

## Code quality
Sonar is used to analyse code quality. You can download sonar from [this link](https://www.sonarqube.org/downloads/) and start a local Sonar server:

After starting sonar server and building project. Run the following command

    mvn sonar:sonar

# About Project

---------------

### Architecture
  1. Microservices 
  2. Serverless Lambda
  3. Pub/Sub Model to Decouple services [ Asynchronous communication ]
### Following three services are part of this project
1. Automated Irrigation System [ Consists of REST APIs for CRUD and ]
    * Microservice
      * Rest API [ For CRUD operations ]
      * Cron Job [ For notifying sensor if irrigation time of some land reaches ]
      * SQS  [ To subscribe sensor notification failure event and resend to sensor ]
2. Sensor Service [ Processes the request and returns success/failure response ]
    * Serverless Lambda
3. Retry/Alert Service [ Multiple retries to sensor service. **Alert raised in case all retries failed** ]
    * AWS SQS [ To publish sensor notification failure event ]
    * AWS SNS [ To raise alert if all retries fail ]

### Tools/Technologies/Libraries Used 
  - [x] Java 8
  - [x] PostgreSQL 
  - [x] Liquibase
  - [x] Spring Caching
  - [x] Spring Actuator
  - [x] Spring Pagination
  - [x] Spring Data JPA
  - [x] ACID Property using Transactional
  - [x] AOP Implemented [For logging HTTP Request and Response]
  - [x] OpenApi Specification/Swagger
  - [x] AWS API Gateway to expose Sensor Lambda